package com.connor.timeout;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageRequest;

import java.util.Optional;
import java.util.UUID;


public class RequestHandlerApiGatewayImpl implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
    private static final String QUEUE_URL_ENV_KEY = "QUEUE_URL";

    private final AmazonSQS amazonSQS;

    public RequestHandlerApiGatewayImpl(AmazonSQS amazonSQS) {
        this.amazonSQS = amazonSQS;
    }

    public RequestHandlerApiGatewayImpl(){
        this(AmazonSQSClientBuilder.defaultClient());
    }

    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent input, final Context context) {
        final String id = UUID.randomUUID().toString();
        final String body = String.format("{\"id\":\"%s\"}", id);
        final String queueUrl = Optional.ofNullable(System.getenv(QUEUE_URL_ENV_KEY)).orElseThrow(() -> new RuntimeException(String.format("Please set the queue url in the %s env var", QUEUE_URL_ENV_KEY)));
        amazonSQS.sendMessage(new SendMessageRequest().withMessageBody(body).withQueueUrl(queueUrl));
        return new APIGatewayProxyResponseEvent().withStatusCode(200).withBody(body);
    }
}
