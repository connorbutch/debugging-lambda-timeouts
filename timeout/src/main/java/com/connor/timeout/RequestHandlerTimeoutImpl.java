package com.connor.timeout;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.amazonaws.xray.AWSXRay;
import com.amazonaws.xray.entities.Subsegment;
import com.google.gson.Gson;


public class RequestHandlerTimeoutImpl implements RequestHandler<SQSEvent, Void> { //TODO change event type to sqs
    private final Gson gson;

    public RequestHandlerTimeoutImpl(Gson gson) {
        this.gson = gson;
    }

    public RequestHandlerTimeoutImpl() {
        this(new Gson());
    }


    public Void handleRequest(final SQSEvent input, final Context context) {
        System.out.printf("Triggered with event %s", input);
        //as long as the subsegment closes, you will get a trace.  But in many cases, a timeout prevents
        //close from being executed.  The commented out code would actually produce a trace in x-ray
//        input.getRecords().stream().map(sqsMessage -> gson.fromJson(sqsMessage.getBody(), Example.class)).forEach(example -> {
//            try (Subsegment subsegment = AWSXRay.beginSubsegment("exampleSubsegment" + example.getId())) {
//                subsegment.putAnnotation("id", example.getId()); //TODO determine if this publishes, since it hits the end of try with resources.....
//            }
//        });
        try (Subsegment subsegment = AWSXRay.beginSubsegment("exampleSubsegment")) {
            subsegment.putAnnotation("id", gson.fromJson(input.getRecords().get(0).getBody(), Example.class).getId());
            while (true) {

            }
        }
    }
}
